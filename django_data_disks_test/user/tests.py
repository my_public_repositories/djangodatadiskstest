from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User


# Index page
class ConnexionPageTestCase(TestCase):
    def test_connexion_page(self):
        # test that connexion page exist (returns a 200)
        response = self.client.get(reverse('user:connexion'))
        self.assertEqual(response.status_code, 200)


# test that deconnexion page returns a 200
class DeconnexionPageTestCase(TestCase):
    # if we are not connected, the page is redirected
    def test_deconnexion_page(self):
        response = self.client.get(reverse('user:deconnexion'))
        self.assertEqual(response.status_code, 302)
        # we should be redirect to the root of the site
        self.assertEqual(response.url, '/')


class ConnexionFormTestCase(TestCase):
    def test_login(self):
        # create a user
        self.user = User.objects.create_user(username='testuser', password='12345')
        # login should works
        login = self.client.login(username='testuser', password='12345')
        self.assertEqual(login, True)
        # with correct logs, we are redirected
        response = self.client.post(reverse('user:connexion'), data={'username': 'testuser',  'password': '12345'})
        self.assertEqual(response.status_code, 302)
        # we should be redirect to the root of the site
        self.assertEqual(response.url, '/')
        # with wrong logs, we are not redirected
        response = self.client.post(reverse('user:connexion'), data={'username': 'toto',  'password': 'toto'})
        self.assertEqual(response.status_code, 200)
