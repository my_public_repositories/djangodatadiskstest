""" module user apps """
from django.apps import AppConfig


class UserConfig(AppConfig):
    """ UserConfig from AppConfig"""
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'user'
