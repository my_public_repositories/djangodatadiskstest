from rest_framework import viewsets, status
from rest_framework.response import Response
# from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend

from .serializers import DiskSerializer
from disk_manager.models import Disk


class DiskViewSet(viewsets.ModelViewSet):
    queryset = Disk.objects.all().order_by('id')
    filter_backends = [DjangoFilterBackend]
    serializer_class = DiskSerializer
    filter_fields = ('user',)

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset

    def create(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(user=request.user)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, pk=None):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=False)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    def destroy(self, request, pk=None):
        instance = self.queryset.filter(id=pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

# class DiskList(generics.ListAPIView):
#     serializer_class = DiskSerializer

#     def get_queryset(self):
#         """
#         This view should return a list of all the purchases
#         for the currently authenticated user.
#         """
#         user = self.request.user
#         return Disk.objects.filter(purchaser=user).order_by('id')
