from rest_framework import serializers

from disk_manager.models import Disk


class DiskSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Disk
        fields = ('id', 'serial_number', 'type', 'brand', 'price', 'size', 'speed', 'quantity')
