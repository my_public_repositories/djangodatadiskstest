from django.urls import reverse
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase
from disk_manager.models import Disk, DiskBrand, DiskType
# from rest_framework.test import APIRequestFactory


class DiskApiTests(APITestCase):

    # ran create and  log user before each test.
    def setUp(self):
        # create a user
        self.user = User.objects.create_user(username='testuser', password='12345')
        # we should connect first:
        self.client.post(reverse('user:connexion'), data={'username': 'testuser',  'password': '12345'})

    def test_post_create_disk(self):
        """
        Ensure we can create a new disk object.
        """
        # init factory
        # factory = APIRequestFactory()
        # create some disks
        response = self.client.post('/api/disks/', {
            "serial_number": "bbbbb",
            "type": "HDD 2'5",
            "brand": "Toshiba",
            "price": "120.00",
            "size": 4000,
            "speed": 50,
            "quantity": 2
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Disk.objects.count(), 1)
        self.assertEqual(Disk.objects.get().serial_number, 'bbbbb')

    def test_get_root_return_all_disks(self):
        """
        Ensure we can get the disks list.
        """
        # create 2 disks in test database
        Disk.objects.create(
            serial_number='slefhzoefhzeo',
            type=DiskType.SSD.value,
            brand=DiskBrand.SANDISK.value,
            price=200,
            quantity=1,
            size=500,
            speed=10,
            user=self.user
        )
        Disk.objects.create(
            serial_number='dfgdfg',
            type=DiskType.HDD_2.value,
            brand=DiskBrand.SEAGATE.value,
            price=100,
            quantity=2,
            size=5000,
            speed=100,
            user=self.user
        )
        self.assertEqual(Disk.objects.count(), 2)
        response = self.client.get('http://localhost:8000/api/disks/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # test we got our two disks
        self.assertEqual(len(response.data), 2)

    def test_get_unique_disk(self):
        """
        Ensure we can get a unique disk list.
        """
        # create 1 disk in test database
        Disk.objects.create(
            serial_number='slefhzoefhzeo',
            type=DiskType.SSD.value,
            brand=DiskBrand.SANDISK.value,
            price=200,
            quantity=1,
            size=500,
            speed=10,
            user=self.user
        )
        self.assertEqual(Disk.objects.count(), 1)
        response = self.client.get('http://localhost:8000/api/disks/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['serial_number'], 'slefhzoefhzeo')

    def test_put_modify_disk(self):
        """
        Ensure we can modify a disk.
        """
        # create 1 disk in test database
        Disk.objects.create(
            serial_number='slefhzoefhzeo',
            type=DiskType.SSD.value,
            brand=DiskBrand.SANDISK.value,
            price=200,
            quantity=1,
            size=500,
            speed=10,
            user=self.user
        )
        self.assertEqual(Disk.objects.count(), 1)
        response = self.client.get('http://localhost:8000/api/disks/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['serial_number'], 'slefhzoefhzeo')
        # change serial number
        response.data['serial_number'] = 'another_sn'
        response = self.client.put('http://localhost:8000/api/disks/1/', response.data)
        disk = Disk.objects.first()
        # assure serial number change in db
        self.assertEqual(disk.serial_number, 'another_sn')
