from django.forms import ModelForm
from disk_manager.models import Disk


class DiskForm(ModelForm):
    class Meta:
        model = Disk
        fields = ('serial_number', 'type', 'brand', 'price', 'size', 'speed', 'quantity',)
