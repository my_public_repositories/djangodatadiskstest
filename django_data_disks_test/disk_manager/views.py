from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponse
from django.db.models import Sum
from django.db.models import F
from disk_manager.forms import DiskForm
from disk_manager.models import Disk
from disk_manager.models import DiskType


def index(request):
    user = request.user
    if user.is_authenticated:
        # filter user disks and add a column with line price (quantity x disks number)
        disks = Disk.objects.filter(user=request.user).order_by('id').annotate(line_price=F('price') * F('quantity'))

        # count value by disk type
        total_by_type = {}
        for current_type in DiskType:
            disks_by_type = disks.filter(type=current_type.value)
            # we count only existing values
            if(disks_by_type.count() != 0):
                total_by_type[current_type.value] = {
                    'total_price': str(disks_by_type.aggregate(sum=Sum('line_price'))['sum']),
                    'disks_quantity': str(disks_by_type.aggregate(sum=Sum('quantity'))['sum']),
                }

        for type, total in total_by_type.items():
            print(f"{type}: {total['total_price']}€, {total['disks_quantity']} disks")

        data = {
            'disks': disks,
            'total_by_type': total_by_type,
            'total_price': str(disks.aggregate(sum=Sum('line_price'))['sum']),
            'disks_quantity': str(disks.aggregate(sum=Sum('quantity'))['sum']),
        }
        return render(request, 'disk_manager/index.html', data)
    else:
        return redirect('user/connexion')


def edit(request, id=None):
    if id:
        member = get_object_or_404(Disk, pk=id)  # load object
    else:
        member = Disk()  # or create new
    if request.method == 'POST':  # when POST request
        form = DiskForm(request.POST, instance=member)  # generate form
        if form.is_valid():  # record if validation is OK
            disk = form.save(commit=False)
            disk.user = request.user
            disk.save()
            return redirect('disk_manager:index')
        else:
            error = form.errors
            print(error)
    else:  # when GET method
        form = DiskForm(instance=member)  # generate form
    return render(request, 'disk_manager/edit.html', dict(form=form, id=id))  # display new or edit page


def delete(request, id):
    # return HttpResponse("Effacer")
    disk = get_object_or_404(Disk, pk=id)
    disk.delete()
    return redirect('disk_manager:index')


def detail(request, id=None):
    return HttpResponse("Détails")
