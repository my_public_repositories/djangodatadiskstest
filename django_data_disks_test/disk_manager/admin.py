from django.contrib import admin
from disk_manager.models import Disk

admin.site.register(Disk)
