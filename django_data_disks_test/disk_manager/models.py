from enum import Enum
from django.db import models
from django.conf import settings


class DiskBrand(Enum):
    TOSHIBA = "Toshiba"
    SEAGATE = "Seagate"
    SANDISK = "SanDisk"
    WD = "Western Digital"


class DiskType(Enum):
    SSD = "SSD"
    SSHD = "SSHD"
    HDD_2 = "HDD 2'5"
    HDD_3 = "HDD 3'5"


class Disk(models.Model):
    serial_number = models.CharField(max_length=255)
    type = models.CharField(
      max_length=15,
      choices=[(tag.value, tag.value) for tag in DiskType]
    )
    brand = models.CharField(
      max_length=15,
      choices=[(tag.value, tag.value) for tag in DiskBrand]
    )
    price = models.DecimalField(max_digits=15, decimal_places=2)  # in €
    quantity = models.IntegerField()
    size = models.IntegerField()  # in Go
    speed = models.IntegerField()  # unit is MB/s
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
