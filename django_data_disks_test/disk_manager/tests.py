from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User
from disk_manager.models import Disk, DiskBrand, DiskType


# Index page
class IndexPageTestCase(TestCase):
    # If we are noc connected we should be redirected to login page
    def test_index_page(self):
        response = self.client.get(reverse('disk_manager:index'))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, 'user/connexion')

    # test we got the client's disks list
    def test_index_page_content(self):
        # create a user
        self.user = User.objects.create_user(username='testuser', password='12345')
        # we should connect first:
        self.client.post(reverse('user:connexion'), data={'username': 'testuser',  'password': '12345'})
        # create some disks
        Disk.objects.create(
            serial_number='slefhzoefhzeo',
            type=DiskType.SSD.value,
            brand=DiskBrand.SANDISK.value,
            price=200,
            quantity=1,
            size=500,
            speed=10,
            user=self.user
        )
        Disk.objects.create(
            serial_number='dfgdfg',
            type=DiskType.HDD_2.value,
            brand=DiskBrand.SEAGATE.value,
            price=100,
            quantity=2,
            size=5000,
            speed=100,
            user=self.user
        )
        response = self.client.get(reverse('disk_manager:index'))
        # test that we are on the good page
        self.assertEqual(response.status_code, 200)
        # test content features:
        # price sum
        content = str(response.content)
        self.assertEqual("400" in content, True)
        # quantity sum
        self.assertEqual("3" in content, True)
        # brands are present
        self.assertEqual(DiskBrand.SEAGATE.value in content, True)
        self.assertEqual(DiskBrand.SANDISK.value in content, True)
        # types are present
        self.assertEqual(DiskType.SSD.value in content, True)
        self.assertEqual(DiskType.HDD_2.value in content, True)
