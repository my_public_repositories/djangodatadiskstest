# Dockerfile

FROM python:3.10-buster

# install nginx
RUN apt-get update && apt-get install git nginx vim -y --no-install-recommends

# Install pipenv and compilation dependencies
RUN python -m pip install --upgrade pip
RUN git clone https://gitlab.com/my_public_repositories/djangodatadiskstest.git
WORKDIR /djangodatadiskstest
RUN cp nginx.default /etc/nginx/sites-available/default
RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log

# copy source and install dependencies
RUN mkdir -p /opt/app
RUN cp requirements.txt /opt/app/
RUN cp startserver.sh /opt/app/
RUN cp -r django_data_disks_test /opt/app/django_data_disks_test/
WORKDIR /opt/app
RUN pip install -r requirements.txt --cache-dir /opt/app/pip_cache
RUN chown -R www-data:www-data /opt/app

# start server
EXPOSE 8080
STOPSIGNAL SIGTERM
CMD ["/opt/app/start-server.sh"]
