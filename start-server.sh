#!/usr/bin/env bash
# start-server.sh
if [ -n "$DJANGO_SUPERUSER_USERNAME" ] && [ -n "$DJANGO_SUPERUSER_PASSWORD" ] ; then
    (cd django_data_disks_test; python manage.py createsuperuser --no-input)
fi
(cd django_data_disks_test; gunicorn django_data_disks_test.wsgi --user www-data --bind 0.0.0.0:8080 --workers 1) &
nginx -g "daemon off;"
