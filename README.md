# DjangoDataDisksTest

Here come my test result for the django developer job at Scaleway

## Getting started

Dependencies are handle with poetry. At first, install it!
`curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -`
Then take a look at the pyproject.toml file. You can install dependencies by usilge:
`poetry install`
Then launch every command with poetry run. For
exemple: `poetry run flake8 django_data_disks_test/`

## git hooks

I use pre-commit lib to launch control on code before commit. These controls use some lib, read more about usage here:
https://siderlabs.com/blog/about-style-guide-of-python-and-linter-tool-pep8-pyflakes-flake8-haking-pyling-7fdbe163079d/

Run pre-commit install to set up the git hook scripts
`poetry run pre-commit install`
Now pre-commit will run automatically on git commit!
